package id.refactory.topupstorage.repos;

import id.refactory.topupstorage.entities.AccountNumberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountNumberRepo extends JpaRepository<AccountNumberEntity, Long> {
    AccountNumberEntity findByAccountNumber(String accountNumber);

    @Modifying
    @Query(value = "UPDATE tbl_rekening SET saldo = :balance WHERE norek = :accountNumber", nativeQuery = true)
    void updateBalance(@Param("balance") Double balance, @Param("accountNumber") String accountNumber);
}
