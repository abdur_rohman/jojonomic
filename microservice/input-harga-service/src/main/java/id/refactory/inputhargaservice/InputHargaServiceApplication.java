package id.refactory.inputhargaservice;

import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.PWA;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@PWA(name = "Gold Digital", shortName = "E-GOLD")
@NpmPackage(value = "line-awesome", version = "1.3.0")
public class InputHargaServiceApplication implements AppShellConfigurator {
    public static void main(String[] args) {
        SpringApplication.run(InputHargaServiceApplication.class, args);
    }
}
