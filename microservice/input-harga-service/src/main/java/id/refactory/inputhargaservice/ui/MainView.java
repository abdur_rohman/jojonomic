package id.refactory.inputhargaservice.ui;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import id.refactory.inputhargaservice.kafka.KafkaMessage;
import id.refactory.inputhargaservice.kafka.KafkaSender;

@PageTitle("Main")
@Route("")
public class MainView extends VerticalLayout {
    public MainView(KafkaSender kafkaSender) {
        NumberField topUpPriceField = new NumberField();
        topUpPriceField.setLabel("Harga Emas");
        topUpPriceField.setValue(0.0);

        NumberField buybackPriceField = new NumberField();
        buybackPriceField.setLabel("Harga BuyBack");
        buybackPriceField.setValue(0.0);

        Button submitButton = new Button("Input Harga Emas");
        submitButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SUCCESS);

        submitButton.addClickListener(buttonClickEvent -> {
            String reffId = "test_001";
            String adminId = "admin_001";
            KafkaMessage kafkaMessage = new KafkaMessage(
                    1L,
                    System.currentTimeMillis(),
                    reffId,
                    adminId,
                    topUpPriceField.getValue(),
                    buybackPriceField.getValue()
            );
            kafkaSender.sendMessage(kafkaMessage);

            topUpPriceField.setValue(0.0);
            buybackPriceField.setValue(0.0);

            Notification notification = new Notification();
            notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);

            Div text = new Div(new Text("Berhasil melakukan input harga emas"));

            Button closeButton = new Button(new Icon("lumo", "cross"));
            closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
            closeButton.getElement().setAttribute("aria-label", "Close");
            closeButton.addClickListener(event -> {
                notification.close();
            });

            HorizontalLayout layout = new HorizontalLayout(text, closeButton);
            layout.setAlignItems(Alignment.CENTER);

            notification.add(layout);
            notification.open();
        });
        submitButton.addClickShortcut(Key.ENTER);

        add(
                new H1("Input Harga Emas"),
                topUpPriceField,
                buybackPriceField,
                submitButton
        );
    }
}
