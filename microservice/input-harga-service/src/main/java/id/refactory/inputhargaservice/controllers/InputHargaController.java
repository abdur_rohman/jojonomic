package id.refactory.inputhargaservice.controllers;

import id.refactory.inputhargaservice.kafka.KafkaMessage;
import id.refactory.inputhargaservice.kafka.KafkaSender;
import id.refactory.inputhargaservice.requests.InputGoldPriceRequest;
import id.refactory.inputhargaservice.responses.BaseResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "api/")
public class InputHargaController {

    private final KafkaSender kafkaSender;

    public InputHargaController(KafkaSender kafkaSender) {
        this.kafkaSender = kafkaSender;
    }

    @PostMapping(value = "input-harga", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    BaseResponse<String> postGoldPrice(@RequestBody InputGoldPriceRequest inputGoldPriceRequest) {
        String reffId = "test_001";
        KafkaMessage kafkaMessage = new KafkaMessage(
                1L,
                System.currentTimeMillis(),
                reffId,
                inputGoldPriceRequest.getAdminId(),
                inputGoldPriceRequest.getTopUpPrice(),
                inputGoldPriceRequest.getBuyBackPrice()
        );
        kafkaSender.sendMessage(kafkaMessage);
        return BaseResponse.success(reffId, null);
    }
}
