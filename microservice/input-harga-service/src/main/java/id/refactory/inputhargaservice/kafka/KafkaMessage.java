package id.refactory.inputhargaservice.kafka;

public class KafkaMessage {
    private Long id, createdAt;
    private String reffId, adminId;
    private Double topUpPrice, buyBackPrice;

    public KafkaMessage(Long id, Long createdAt, String reffId, String adminId, Double topUpPrice, Double buyBackPrice) {
        this.id = id;
        this.createdAt = createdAt;
        this.reffId = reffId;
        this.adminId = adminId;
        this.topUpPrice = topUpPrice;
        this.buyBackPrice = buyBackPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getReffId() {
        return reffId;
    }

    public void setReffId(String reffId) {
        this.reffId = reffId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public Double getTopUpPrice() {
        return topUpPrice;
    }

    public void setTopUpPrice(Double topUpPrice) {
        this.topUpPrice = topUpPrice;
    }

    public Double getBuyBackPrice() {
        return buyBackPrice;
    }

    public void setBuyBackPrice(Double buyBackPrice) {
        this.buyBackPrice = buyBackPrice;
    }
}
