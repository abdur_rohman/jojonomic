package id.refactory.topupservice.controllers;

import id.refactory.topupservice.responses.AuthorResponse;
import id.refactory.topupservice.responses.BaseResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class IndexController {
    @RequestMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public BaseResponse<AuthorResponse> getIndex() {
        String reffId = "reff001";
        return BaseResponse.success(reffId, new AuthorResponse());
    }
}
