package id.refactory.topupservice.repos;

import id.refactory.topupservice.entities.AccountNumberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountNumberRepo extends JpaRepository<AccountNumberEntity, Long> {
    @Query(value = "SELECT saldo FROM tbl_rekening WHERE norek = :accountNumber LIMIT 1", nativeQuery = true)
    Double getCurrentBalance(@Param("accountNumber") String accountNumber);
}
