package id.refactory.topupservice.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TopUpRequest {
    @JsonProperty(value = "gram")
    Double weight;

    @JsonProperty(value = "harga")
    Double price;

    @JsonProperty(value = "norek")
    String accountNumber;

    @JsonCreator
    public TopUpRequest() {

    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
