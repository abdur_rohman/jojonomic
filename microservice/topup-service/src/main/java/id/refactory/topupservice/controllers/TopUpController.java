package id.refactory.topupservice.controllers;

import id.refactory.topupservice.entities.GoldPriceEntity;
import id.refactory.topupservice.kafka.KafkaMessage;
import id.refactory.topupservice.kafka.KafkaSender;
import id.refactory.topupservice.repos.AccountNumberRepo;
import id.refactory.topupservice.repos.GoldPriceRepo;
import id.refactory.topupservice.requests.TopUpRequest;
import id.refactory.topupservice.responses.BaseResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "api/")
public class TopUpController {

    private final KafkaSender kafkaSender;
    private final GoldPriceRepo goldPriceRepo;
    private final AccountNumberRepo accountNumberRepo;

    public TopUpController(KafkaSender kafkaSender, GoldPriceRepo goldPriceRepo, AccountNumberRepo accountNumberRepo) {
        this.kafkaSender = kafkaSender;
        this.goldPriceRepo = goldPriceRepo;
        this.accountNumberRepo = accountNumberRepo;
    }

    @PostMapping(value = "topup", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    BaseResponse<String> topUp(@RequestBody TopUpRequest topUpRequest) {
        GoldPriceEntity goldPriceEntity = goldPriceRepo.findFirstByOrderByIdDesc();

        String reffId = "test_001";
        if (!goldPriceEntity.getTopUpPrice().equals(topUpRequest.getPrice())) {
            return BaseResponse.error(reffId);
        }

        String type = "topup";
        String accountNumber = topUpRequest.getAccountNumber();
        Double currentBalance = accountNumberRepo.getCurrentBalance(accountNumber);
        if (currentBalance == null) currentBalance = 0.0;

        KafkaMessage kafkaMessage = new KafkaMessage(
                1L,
                System.currentTimeMillis(),
                reffId,
                accountNumber,
                type,
                currentBalance,
                topUpRequest.getWeight(),
                goldPriceEntity.getTopUpPrice(),
                goldPriceEntity.getBuyBackPrice()
        );

        kafkaSender.sendMessage(kafkaMessage);

        return BaseResponse.success(reffId, null);
    }
}
