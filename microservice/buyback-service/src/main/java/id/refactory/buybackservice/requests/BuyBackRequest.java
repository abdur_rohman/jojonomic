package id.refactory.buybackservice.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BuyBackRequest {
    @JsonProperty(value = "norek")
    String accountNumber;

    @JsonProperty(value = "harga")
    Double price;

    @JsonProperty(value = "gram")
    Double weight;

    @JsonCreator
    public BuyBackRequest() {

    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
