package id.refactory.buybackservice.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BuyBackResponse {
    @JsonProperty(value = "norek")
    private String accountNumber;

    @JsonProperty(value = "saldo")
    private Double balance;

    @JsonProperty(value = "gram")
    private Double weight;

    @JsonCreator
    public BuyBackResponse(String accountNumber, Double balance, Double weight) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.weight = weight;
    }
}
