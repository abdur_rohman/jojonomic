package id.refactory.buybackservice.entities;

import javax.persistence.*;

@Entity
@Table(name = "tbl_rekening")
public class AccountNumberEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "norek", unique = true)
    private String accountNumber;

    @Column(name = "saldo")
    private Double balance;

    @Column(name = "created_at")
    private Long createdAt;

    public AccountNumberEntity() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }
}
