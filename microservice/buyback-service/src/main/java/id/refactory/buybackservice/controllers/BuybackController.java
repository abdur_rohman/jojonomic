package id.refactory.buybackservice.controllers;

import id.refactory.buybackservice.entities.GoldPriceEntity;
import id.refactory.buybackservice.kafka.KafkaMessage;
import id.refactory.buybackservice.kafka.KafkaSender;
import id.refactory.buybackservice.repos.AccountNumberRepo;
import id.refactory.buybackservice.repos.GoldPriceRepo;
import id.refactory.buybackservice.requests.BuyBackRequest;
import id.refactory.buybackservice.responses.BaseResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "api/")
public class BuybackController {

    private final KafkaSender kafkaSender;
    private final GoldPriceRepo goldPriceRepo;
    private final AccountNumberRepo accountNumberRepo;

    public BuybackController(KafkaSender kafkaSender, GoldPriceRepo goldPriceRepo, AccountNumberRepo accountNumberRepo) {
        this.kafkaSender = kafkaSender;
        this.goldPriceRepo = goldPriceRepo;
        this.accountNumberRepo = accountNumberRepo;
    }

    @PostMapping(value = "buyback", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    BaseResponse<String> topUp(@RequestBody BuyBackRequest buyBackRequest) {
        GoldPriceEntity goldPriceEntity = goldPriceRepo.findFirstByOrderByIdDesc();

        String reffId = "test_001";
        String type = "buyback";
        String accountNumber = buyBackRequest.getAccountNumber();
        Double currentBalance = accountNumberRepo.getCurrentBalance(accountNumber);
        if (currentBalance == null) currentBalance = 0.0;

        if (currentBalance < buyBackRequest.getWeight()) {
            return BaseResponse.error(reffId);
        }

        KafkaMessage kafkaMessage = new KafkaMessage(
                1L,
                System.currentTimeMillis(),
                reffId,
                accountNumber,
                type,
                currentBalance,
                buyBackRequest.getWeight(),
                goldPriceEntity.getTopUpPrice(),
                goldPriceEntity.getBuyBackPrice()
        );

        kafkaSender.sendMessage(kafkaMessage);

        return BaseResponse.success(reffId, null);
    }
}
