package id.refactory.buybackservice.kafka;

public class KafkaMessage {
    private Long id, createdAt;
    private String reffId, accountNumber, type;
    private Double goldBalance, weight, topUpPrice, buyBackPrice;

    public KafkaMessage(Long id, Long createdAt, String reffId, String accountNumber, String type, Double goldBalance, Double weight, Double topUpPrice, Double buyBackPrice) {
        this.id = id;
        this.createdAt = createdAt;
        this.reffId = reffId;
        this.accountNumber = accountNumber;
        this.type = type;
        this.goldBalance = goldBalance;
        this.weight = weight;
        this.topUpPrice = topUpPrice;
        this.buyBackPrice = buyBackPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getReffId() {
        return reffId;
    }

    public void setReffId(String reffId) {
        this.reffId = reffId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getGoldBalance() {
        return goldBalance;
    }

    public void setGoldBalance(Double goldBalance) {
        this.goldBalance = goldBalance;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getTopUpPrice() {
        return topUpPrice;
    }

    public void setTopUpPrice(Double topUpPrice) {
        this.topUpPrice = topUpPrice;
    }

    public Double getBuyBackPrice() {
        return buyBackPrice;
    }

    public void setBuyBackPrice(Double buyBackPrice) {
        this.buyBackPrice = buyBackPrice;
    }
}
