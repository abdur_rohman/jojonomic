package id.refactory.buybackservice.repos;

import id.refactory.buybackservice.entities.GoldPriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoldPriceRepo extends JpaRepository<GoldPriceEntity, Long> {
    GoldPriceEntity findFirstByOrderByIdDesc();
}
