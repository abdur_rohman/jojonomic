package id.refactory.checkmutasiservice.controllers;

import id.refactory.checkmutasiservice.entities.TransactionEntity;
import id.refactory.checkmutasiservice.repos.TransactionRepo;
import id.refactory.checkmutasiservice.requests.CheckMutasiRequest;
import id.refactory.checkmutasiservice.responses.BaseResponse;
import id.refactory.checkmutasiservice.responses.MutasiResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "api/")
public class CheckMutasiController {

    private final TransactionRepo transactionRepo;

    public CheckMutasiController(TransactionRepo transactionRepo) {
        this.transactionRepo = transactionRepo;
    }

    @PostMapping(value = "mutasi", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    BaseResponse<List<MutasiResponse>> topUp(@RequestBody CheckMutasiRequest checkMutasiRequest) {
        List<TransactionEntity> transactionEntities = transactionRepo.findAllByAccountNumberAndCreatedAtAfterAndCreatedAtBefore(
                checkMutasiRequest.getAccountNumber(),
                checkMutasiRequest.getStartDate(),
                checkMutasiRequest.getEndDate()
        );

        List<MutasiResponse> mutasiResponseList = transactionEntities.stream().map(entity ->
                new MutasiResponse(
                        entity.getType(),
                        entity.getBalance(),
                        entity.getWeight(),
                        entity.getTopUpPrice(),
                        entity.getBuyBackPrice(),
                        entity.getCreatedAt()
                )
        ).collect(Collectors.toList());

        String reffId = "test_001";
        return BaseResponse.success(reffId, mutasiResponseList);
    }
}
