package id.refactory.checkmutasiservice.repos;

import id.refactory.checkmutasiservice.entities.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepo extends JpaRepository<TransactionEntity, Long> {
    List<TransactionEntity> findAllByAccountNumberAndCreatedAtAfterAndCreatedAtBefore(String accountNumber, Long startDate, Long endDate);
}
