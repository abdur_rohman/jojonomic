package id.refactory.checkmutasiservice.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseResponse<T> {
    @JsonProperty
    private boolean error;
    @JsonProperty(value = "reff_id")
    private String reffId;
    @JsonProperty
    private String message;
    @JsonProperty
    private T data;

    @JsonCreator
    public BaseResponse(boolean error, String reffId, String message, T data) {
        this.error = error;
        this.reffId = reffId;
        this.message = message;
        this.data = data;
    }

    public static <T> BaseResponse<T> success(String reffId, T data) {
        return new BaseResponse<>(false, reffId, null, data);
    }

    public static <T> BaseResponse<T> error(String reffId) {
        return new BaseResponse<>(true, reffId, "Kafka not ready", null);
    }
}
