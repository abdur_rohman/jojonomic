package id.refactory.checkmutasiservice.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MutasiResponse {
    @JsonProperty(value = "type")
    private String type;

    @JsonProperty(value = "saldo")
    private Double balance;

    @JsonProperty(value = "gram")
    private Double weight;

    @JsonProperty(value = "harga_topup")
    private Double topUpPrice;

    @JsonProperty(value = "harga_buyback")
    private Double buyBackPrice;

    @JsonProperty(value = "date")
    private Long date;

    @JsonCreator
    public MutasiResponse(String type, Double balance, Double weight, Double topUpPrice, Double buyBackPrice, Long date) {
        this.type = type;
        this.balance = balance;
        this.weight = weight;
        this.topUpPrice = topUpPrice;
        this.buyBackPrice = buyBackPrice;
        this.date = date;
    }
}
