package id.refactory.checkmutasiservice.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckMutasiRequest {
    @JsonProperty(value = "norek")
    String accountNumber;

    @JsonProperty(value = "start_date")
    Long startDate;

    @JsonProperty(value = "end_date")
    Long endDate;

    @JsonCreator
    public CheckMutasiRequest() {

    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }
}
