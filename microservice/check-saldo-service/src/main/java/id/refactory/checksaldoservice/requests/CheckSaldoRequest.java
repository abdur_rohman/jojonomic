package id.refactory.checksaldoservice.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckSaldoRequest {
    @JsonProperty(value = "norek")
    String accountNumber;

    @JsonCreator
    public CheckSaldoRequest() {

    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
