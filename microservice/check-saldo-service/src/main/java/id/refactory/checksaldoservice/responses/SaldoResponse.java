package id.refactory.checksaldoservice.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SaldoResponse {
    @JsonProperty(value = "norek")
    private String accountNumber;

    @JsonProperty(value = "saldo")
    private Double balance;

    @JsonCreator
    public SaldoResponse(String accountNumber, Double balance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }
}
