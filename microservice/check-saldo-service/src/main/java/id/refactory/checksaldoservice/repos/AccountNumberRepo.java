package id.refactory.checksaldoservice.repos;

import id.refactory.checksaldoservice.entities.AccountNumberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountNumberRepo extends JpaRepository<AccountNumberEntity, Long> {
    AccountNumberEntity findByAccountNumber(String accountNumber);
}
