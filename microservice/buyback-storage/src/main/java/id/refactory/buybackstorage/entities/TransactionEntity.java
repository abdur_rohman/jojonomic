package id.refactory.buybackstorage.entities;

import javax.persistence.*;

@Entity
@Table(name = "tbl_transaksi")
public class TransactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "norek")
    private String accountNumber;

    @Column(name = "type")
    private String type;

    @Column(name = "gram")
    private Double weight;

    @Column(name = "harga_topup")
    private Double topUpPrice;

    @Column(name = "harga_buyback")
    private Double buyBackPrice;

    @Column(name = "saldo")
    private Double balance;

    @Column(name = "created_at")
    private Long createdAt;

    public TransactionEntity() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Double getTopUpPrice() {
        return topUpPrice;
    }

    public void setTopUpPrice(Double topUpPrice) {
        this.topUpPrice = topUpPrice;
    }

    public Double getBuyBackPrice() {
        return buyBackPrice;
    }

    public void setBuyBackPrice(Double buyBackPrice) {
        this.buyBackPrice = buyBackPrice;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
