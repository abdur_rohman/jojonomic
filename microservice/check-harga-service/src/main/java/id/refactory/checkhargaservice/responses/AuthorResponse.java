package id.refactory.checkhargaservice.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorResponse {
    @JsonProperty
    private final String name = "Abdur Rohman";
    @JsonProperty
    private final String github = "https://github.com/abdur-rohman";
    @JsonProperty
    private final String linkedin = "https://www.linkedin.com/in/abdur-rohman-2b1455140";
    @JsonProperty("stack_overflow")
    private final String stackOverflow = "https://stackoverflow.com/users/7302514/abdur-rohman";

    @JsonCreator
    public AuthorResponse() {

    }
}
