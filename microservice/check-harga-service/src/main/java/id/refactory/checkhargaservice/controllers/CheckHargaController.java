package id.refactory.checkhargaservice.controllers;

import id.refactory.checkhargaservice.entities.GoldPriceEntity;
import id.refactory.checkhargaservice.repos.GoldPriceRepo;
import id.refactory.checkhargaservice.responses.BaseResponse;
import id.refactory.checkhargaservice.responses.GoldPriceResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "api/")
public class CheckHargaController {

    private final GoldPriceRepo goldPriceRepo;

    public CheckHargaController(GoldPriceRepo goldPriceRepo) {
        this.goldPriceRepo = goldPriceRepo;
    }

    @GetMapping(value = "check-harga", produces = APPLICATION_JSON_VALUE)
    BaseResponse<GoldPriceResponse> getLastGoldPrice() {
        GoldPriceEntity goldPriceEntity = goldPriceRepo.findFirstByOrderByIdDesc();
        GoldPriceResponse goldPriceResponse = new GoldPriceResponse(
                goldPriceEntity.getBuyBackPrice(),
                goldPriceEntity.getTopUpPrice()
        );

        return BaseResponse.success(null, goldPriceResponse);
    }
}
