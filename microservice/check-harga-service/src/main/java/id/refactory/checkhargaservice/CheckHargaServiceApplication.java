package id.refactory.checkhargaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckHargaServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CheckHargaServiceApplication.class, args);
    }

}
