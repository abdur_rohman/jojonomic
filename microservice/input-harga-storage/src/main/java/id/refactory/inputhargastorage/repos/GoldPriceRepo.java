package id.refactory.inputhargastorage.repos;

import id.refactory.inputhargastorage.entities.GoldPriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoldPriceRepo extends JpaRepository<GoldPriceEntity, Long> {

}
