package id.refactory.inputhargastorage.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_harga")
public class GoldPriceEntity {
    @Id
    private Long id;

    @Column(name = "admin_id")
    private String adminId;

    @Column(name = "harga_topup")
    private Double topUpPrice;

    @Column(name = "harga_buyback")
    private Double buyBackPrice;

    @Column(name = "created_at")
    private Long createdAt;

    public GoldPriceEntity() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public Double getTopUpPrice() {
        return topUpPrice;
    }

    public void setTopUpPrice(Double topUpPrice) {
        this.topUpPrice = topUpPrice;
    }

    public Double getBuyBackPrice() {
        return buyBackPrice;
    }

    public void setBuyBackPrice(Double buyBackPrice) {
        this.buyBackPrice = buyBackPrice;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }
}
