package id.refactory.inputhargastorage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.refactory.inputhargastorage.entities.GoldPriceEntity;
import id.refactory.inputhargastorage.kafka.KafkaMessage;
import id.refactory.inputhargastorage.repos.GoldPriceRepo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class InputHargaStorageApplication {
    private final GoldPriceRepo goldPriceRepo;

    public InputHargaStorageApplication(GoldPriceRepo goldPriceRepo) {
        this.goldPriceRepo = goldPriceRepo;
    }

    public static void main(String[] args) {
        SpringApplication.run(InputHargaStorageApplication.class, args);
    }

    @KafkaListener(topics = "input-harga", groupId = "e_gold")
    public void listen(String message) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            KafkaMessage kafkaMessage = objectMapper.readValue(message, KafkaMessage.class);

            GoldPriceEntity goldPriceEntity = new GoldPriceEntity();
            goldPriceEntity.setId(kafkaMessage.getId());
            goldPriceEntity.setTopUpPrice(kafkaMessage.getTopUpPrice());
            goldPriceEntity.setBuyBackPrice(kafkaMessage.getBuyBackPrice());
            goldPriceEntity.setAdminId(kafkaMessage.getAdminId());
            goldPriceEntity.setCreatedAt(kafkaMessage.getCreatedAt());

            goldPriceRepo.save(goldPriceEntity);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
