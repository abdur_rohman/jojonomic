## E-GOLD

Aplikasi Emas Digital

### Jalankan docker compose sebelum run project ini

```bash
docker-compose -f ./misc/docker-compose.yaml up -d
```

### Environment Variable

```bash
export E_GOLD_DB_USERNAME={$s3cr3t}
export E_GOLD_DB_PASSWORD={$s3cr3t}
export E_GOLD_DB_HOSTNAME={$s3cr3t}
export E_GOLD_DB_PORT={$s3cr3t}
export E_GOLD_DB_NAME={$s3cr3t}
export E_GOLD_KAFKA_BOOSTRAP_SERVER={$s3cr3t}
```

### Hasil

![Image 1](./evidence/1.png)
![Image 2](./evidence/2.png)
![Image 3](./evidence/3.png)
![Image 4](./evidence/4.png)
![Image 5](./evidence/5.png)
![Image 6](./evidence/6.png)
![Image 7](./evidence/7.png)
![Image 8](./evidence/8.png)
![Image 9](./evidence/9.png)

Postman collection ada di folder `misc`